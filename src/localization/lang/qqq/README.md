## What language is this?

qqq is not a language. It serves as documentation for the keys. If you want to translate to another language, look in the en-US folder for translated keys, and qqq for what they mean/where they are used.
