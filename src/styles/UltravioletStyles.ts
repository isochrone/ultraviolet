import MaterialStyle from "./material/Material";
import Style from "./Style";

declare global {
    // noinspection JSUnusedGlobalSymbols
    interface Window {
        UltravioletStyles: Style[];
    }
}

// Do not lazy-load the Material style!
export const DefaultAppStyles = [MaterialStyle];

export default window.UltravioletStyles;
