import {
    DisplayInformation,
    DisplayInformationOption,
    Setting,
    UIInputType,
} from "app/config/user/Setting";
import { UVUIPreferencesItem } from "app/ui/elements/UVUIPreferencesItem";
import { h } from "@10nm/tsx-dom";
import MaterialRadioField from "app/styles/material/ui/components/MaterialRadioField";
import MaterialSelect from "app/styles/material/ui/components/MaterialSelect";
import MaterialTextInput from "app/styles/material/ui/components/MaterialTextInput";
import StyleManager from "app/styles/StyleManager";
import MaterialSwitch from "./components/MaterialSwitch";
import Log from "app/data/AppLog";
import { getStyleMeta } from "app/styles/Style";
import MaterialCheckbox, {
    MaterialCheckboxTrack,
} from "./components/MaterialCheckbox";
import i18next from "i18next";
import { generateId } from "app/util";
import PageIcons, {
    isPageIconEnabled,
    PageIcon,
} from "app/ui/definitions/PageIcons";
import MaterialIcon from "app/styles/material/ui/components/MaterialIcon";
import Sortable from "sortablejs";
import { SortableList } from "app/styles/material/ui/components/SortableList";

/**
 * The MaterialPreferencesItem is a handling class used for different items in the preferences page.
 */
export default class MaterialPreferencesItem extends UVUIPreferencesItem {
    /** Input element */
    private input: HTMLElement | HTMLElement[];

    /**
     * Handles onChange event of the input element.
     */
    handleInputChange(value: any): void {
        this.result = value;
        // check for onChange function
        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    renderInputElement(): HTMLElement | HTMLElement[] {
        switch (this.props.setting.displayInfo.uiInputType) {
            case UIInputType.Switch:
                this.input = this.renderSwitchInput();
                break;
            case UIInputType.Checkbox:
                this.input = this.renderCheckboxInput();
                break;
            case UIInputType.Checkboxes:
                // TODO: Implement checkboxes
                break;
            case UIInputType.Radio:
                this.input = this.renderRadioInput();
                break;
            case UIInputType.Dropdown:
                this.input = this.renderDropdownInput();
                break;
            case UIInputType.Textbox:
                this.input = this.renderTextInput();
                break;
            case UIInputType.Number:
                this.input = this.renderNumberInput();
                break;
            case UIInputType.ColorPicker:
                // TODO: Implement color picker
                break;
            case UIInputType.Style:
            case UIInputType.Theme:
                this.input = this.renderThemeInput();
                break;
            case UIInputType.RevertOptions:
                // TODO: Implement revert options
                break;
            case UIInputType.PageIcons:
                this.input = this.renderPageIconsInput();
                break;
        }
        return (this.input =
            this.input ??
            ((
                <span style="font-weight: bold">
                    This setting is currently unsupported.
                </span>
            ) as HTMLElement));
    }

    renderSwitchInput(): HTMLElement | HTMLElement[] {
        return Array.from(
            (
                <MaterialSwitch
                    default={(this.props.setting as Setting<boolean>).value}
                    onChange={(value) => this.handleInputChange(value)}
                />
            ).children
        ) as HTMLElement[];
    }

    renderCheckboxInput(): HTMLElement | HTMLElement[] {
        const id = generateId();
        return [
            (
                <MaterialCheckbox
                    id={id}
                    default={(this.props.setting as Setting<boolean>).value}
                    onChange={(value) => this.handleInputChange(value)}
                />
            ) as HTMLElement,
            (
                <label for={id}>{i18next.t<string>("ui:toggleCheckbox")}</label>
            ) as HTMLElement,
        ];
    }

    renderRadioInput(): HTMLElement | HTMLElement[] {
        return (
            <MaterialRadioField<DisplayInformationOption>
                radios={(
                    this.props.setting.displayInfo as DisplayInformation & {
                        uiInputType: UIInputType.Radio;
                    }
                ).validOptions.map((options) => ({
                    value: options.value,
                    children: <span>{options.name ?? options.value}</span>,
                    checked: options.value === this.props.setting.value,
                }))}
                direction={"vertical"}
                onChange={(value) => this.handleInputChange(value)}
            />
        ) as HTMLElement;
    }

    renderDropdownInput(): HTMLElement | HTMLElement[] {
        return (
            <MaterialSelect<DisplayInformationOption>
                items={(
                    this.props.setting.displayInfo as DisplayInformation & {
                        uiInputType: UIInputType.Dropdown;
                    }
                ).validOptions.map((options) => ({
                    type: "action",
                    label: options.name,
                    value: options,
                    selected: options.value === this.props.setting.value,
                }))}
                label={this.props.setting.displayInfo.title}
                onChange={(value) => this.handleInputChange(value)}
            />
        ) as HTMLElement;
    }

    renderTextInput(): HTMLElement | HTMLElement[] {
        return (
            <MaterialTextInput
                label={this.props.setting.displayInfo.title}
                // If the value wasn't actually a string, blame whoever made the setting
                // for using the Textbox UIInputType for a string value.
                defaultText={`${this.props.setting.value}`}
                onInput={(value) => this.handleInputChange(value)}
            />
        ) as HTMLElement;
    }

    renderNumberInput(): HTMLElement | HTMLElement[] {
        return (
            <MaterialTextInput
                type="number"
                label={this.props.setting.displayInfo.title}
                // If the value wasn't actually a number, blame whoever made the setting
                // for using the Textbox UIInputType for a number value.
                defaultText={`${this.props.setting.value}`}
                onInput={(value) => this.handleInputChange(value)}
            />
        ) as HTMLElement;
    }

    renderThemeInput(): HTMLElement | HTMLElement[] {
        return (
            <MaterialSelect<string>
                items={StyleManager.styles.map((style) => ({
                    type: "action",
                    label: getStyleMeta(style).displayName,
                    value: style.name,
                    selected: style.name === this.props.setting.value,
                }))}
                label={this.props.setting.displayInfo.title}
                onChange={(value) =>
                    this.handleInputChange(StyleManager.styles[value].name)
                }
            />
        ) as HTMLElement;
    }

    renderPageIconsInput(): HTMLElement | HTMLElement[] {
        // Impossible, as `list` is (in syntax) accessed before it is defined.
        // eslint-disable-next-line prefer-const
        let list: HTMLElement;
        const checkboxIds: Record<string, string> = {};
        const onChange = () => {
            this.handleInputChange(
                Object.fromEntries(
                    Sortable.get(list)
                        .toArray()
                        .map((v) => {
                            const iconId = v.replace("pageIcon--", "");
                            return [
                                iconId,
                                {
                                    enabled: MaterialCheckboxTrack.get(
                                        checkboxIds[iconId]
                                    ).component.checked,
                                    // TODO: Color
                                },
                            ];
                        })
                )
            );
        };

        function pageIconRow(pi: PageIcon) {
            const i = generateId();
            checkboxIds[pi.id] = i;
            return (
                <li data-id={"pageIcon--" + pi.id}>
                    <div class="mdc-form-field">
                        <MaterialIcon
                            class="mdc-list-drag"
                            icon={"drag_indicator"}
                            iconColor={"gray"}
                        />
                        <MaterialCheckbox
                            id={i}
                            default={isPageIconEnabled(pi)}
                            disabled={pi.required}
                            onChange={onChange}
                        />
                        <MaterialIcon icon={pi.icon} iconColor={pi.color} />
                        <label htmlFor={i} style="margin-left: 0.2em;">
                            {pi.name ?? `${i18next.t(`ui:pageIcons.${pi.id}`)}`}
                        </label>
                    </div>
                </li>
            );
        }

        list = (
            <SortableList draggableHandle={".mdc-list-drag"}>
                {PageIcons().map((pi) => pageIconRow(pi))}
            </SortableList>
        ) as HTMLElement;

        Sortable.get(list).option("onUpdate", onChange);

        return list;
    }

    render(): HTMLDivElement {
        Log.trace("Rendering MaterialPreferencesItem", { props: this.props });
        this.renderInputElement();
        return (this.element = (
            <div class={"uv-mdc-preference"}>
                <h2>{this.props.setting.displayInfo.title}</h2>
                <p
                    dangerouslySetInnerHTML={
                        /* Since this value comes from i18n, we assume that it has already been properly sanitised. */
                        /* In some cases we may need a link in the description (e.g. revert/rtrcAutoDiff)*/
                        this.props.setting.displayInfo.description
                    }
                ></p>
                <div class={`mdc-form-field`}>{this.input}</div>
            </div>
        ) as HTMLDivElement);
    }
}
