import UVUIElement from "app/ui/elements/UVUIElement";

export abstract class MaterialWarnDialogChild extends UVUIElement {
    /**
     * Refresh the contents of this child without changing the root.
     */
    abstract refresh(): void;
}
