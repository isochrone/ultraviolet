import { BaseProps, h } from "@10nm/tsx-dom";
import classMix from "app/styles/material/util/classMix";
import i18next from "i18next";
import MaterialTextInput, {
    MaterialTextInputComponents,
    MaterialTextInputUpgrade,
} from "app/styles/material/ui/components/MaterialTextInput";
import MaterialIconButton from "app/styles/material/ui/components/MaterialIconButton";

import "../../css/materialInputCard.css";
import UltravioletUI from "app/ui/UltravioletUI";
import Log from "app/data/AppLog";

export enum MaterialInputCardState {
    Blank,
    Input,
    Loading,
    Ready,
}

export interface MaterialInputCardProps extends BaseProps {
    label: string;
    value?: string;
    class?: string;
    outlined?: boolean;

    submitIcon?: string;
    cancelIcon?: string;

    i18n?: {
        loadingText?: string;
        inputSubmit?: string;
        inputCancel?: string;
        apiError?: string;
    };
}

/**
 * The MaterialInputCard is an abstract class used to create cards whose
 * values may be modified at any point.
 */
export default abstract class MaterialInputCard {
    value: string;
    readonly defaultValue: string;
    readonly props: MaterialInputCardProps;

    private _state: MaterialInputCardState = MaterialInputCardState.Blank;
    get state(): MaterialInputCardState {
        return this._state;
    }
    set state(value: MaterialInputCardState) {
        this._state = value;
        if (!!this.elementSet.root) {
            this.elementSet.root.setAttribute(
                "data-state",
                MaterialInputCardState[value].toLowerCase()
            );
        }
    }

    private elementSet: Partial<{
        root: HTMLElement;
        input: HTMLElement;
        inputBox: {
            element: HTMLElement;
            components: MaterialTextInputComponents;
        };
        inputSubmit: HTMLElement;
        inputCancel: HTMLElement;
        loading: HTMLElement;
        loadingText: HTMLElement;
        main: HTMLElement;
    }> = {};

    protected constructor(props: MaterialInputCardProps) {
        this.props = props;
        this.defaultValue = props.value;
    }

    /**
     * Only called once: when the element is being appended to the document.
     */
    renderInput(): HTMLElement {
        let inputElement: HTMLElement;
        const el = (
            <div class={"uv-mdc-inputCard-input"}>
                {
                    (inputElement = (
                        <MaterialTextInput
                            width={"400px"}
                            label={this.props.label}
                        />
                    ) as HTMLElement)
                }
                {
                    (this.elementSet.inputSubmit = (
                        <MaterialIconButton
                            icon={this.props.submitIcon ?? "send"}
                            onClick={() => {
                                this.change(
                                    this.elementSet.inputBox.components
                                        .textField.value
                                );
                            }}
                            disabled
                            data-uv-mdc-tooltip={this.props.i18n?.inputSubmit}
                        />
                    ) as HTMLElement)
                }
                {
                    (this.elementSet.inputCancel = (
                        <MaterialIconButton
                            icon={this.props.cancelIcon ?? "cancel"}
                            onClick={() => {
                                this.cancelInput();
                            }}
                            data-uv-mdc-tooltip={this.props.i18n?.inputCancel}
                        />
                    ) as HTMLElement)
                }
            </div>
        ) as HTMLElement;

        this.elementSet.inputBox = {
            element: inputElement,
            components: MaterialTextInputUpgrade(inputElement),
        };

        this.elementSet.inputBox.components.textField.listen(
            "keypress",
            (e) => {
                const value =
                    this.elementSet.inputBox.components.textField.value;
                const empty = value.length === 0;
                if (!empty && e.key === "Enter") {
                    this.change(value);
                }

                this.elementSet.inputSubmit.toggleAttribute("disabled", empty);
            }
        );

        return el;
    }

    /**
     * Called whenever the content of the input card has been modified or when
     * the card is loading the default value.
     */
    renderLoading(): HTMLElement {
        return (
            <div class={"uv-mdc-inputCard-loading"}>
                {
                    (this.elementSet.loadingText = (
                        <div class={"uv-mdc-inputCard-loading__title"}>
                            {this.value}
                        </div>
                    ) as HTMLElement)
                }
                <div class={"uv-mdc-inputCard-loading__subtitle"}>
                    {this.props.i18n?.loadingText ??
                        `${i18next.t("common:load")}`}
                </div>
            </div>
        ) as HTMLElement;
    }

    /**
     * Only called once: when the element is being appended to the document.
     */
    render(): HTMLElement {
        this.elementSet.root = (
            <div
                class={classMix(
                    "uv-mdc-inputCard",
                    "mdc-card",
                    this.props.outlined ? "mdc-card--outlined" : false,
                    ...(this.props.class ?? [])
                )}
            >
                {
                    (this.elementSet.main = (
                        <div class="uv-mdc-inputCard-main" />
                    ) as HTMLElement)
                }
                {(this.elementSet.input = this.renderInput())}
                {(this.elementSet.loading = this.renderLoading())}
            </div>
        ) as HTMLElement;

        if (!!this.defaultValue) {
            // Default value given, load it in first.
            this.change(this.defaultValue);
        } else {
            // Need to present options.
            this.beginInput();
        }

        return this.elementSet.root;
    }

    replaceMain(newMain: HTMLElement): void {
        if (this.elementSet.main == null) return;
        this.elementSet.main.parentElement.replaceChild(
            newMain,
            this.elementSet.main
        );
        newMain.classList.toggle("uv-mdc-inputCard-main", true);
        this.elementSet.main = newMain;
    }

    /**
     * Hoists the input screen.
     */
    beginInput(): void {
        if (this.value) {
            this.elementSet.inputSubmit.toggleAttribute("disabled", false);
            this.elementSet.inputCancel.style.display = "";
            this.elementSet.inputBox.components.textField.value = this.value;
        } else this.elementSet.inputCancel.style.display = "none";
        this.state = MaterialInputCardState.Input;
        this.elementSet.inputBox.components.textField.focus();
    }

    /**
     * Cancels input and immediately returns to the main screen.
     */
    cancelInput(): void {
        this.state = MaterialInputCardState.Ready;
    }

    /**
     * Changes the current value and hoists the loading screen if a blocking
     * task is being done.
     *
     * @param newValue The new value of the card.
     */
    change(newValue: string): void {
        if (newValue === this.value) {
            this.state = MaterialInputCardState.Ready;
            return;
        }

        this.value = newValue;
        this.elementSet.inputCancel.toggleAttribute("disabled", false);

        const newMain = this.renderMain(newValue);
        if (newMain instanceof Promise) {
            this.state = MaterialInputCardState.Loading;
            this.elementSet.loadingText.innerText = newValue;
            newMain
                .then((element: HTMLElement) => {
                    this.replaceMain(element);
                    this.state = MaterialInputCardState.Ready;
                })
                .catch((e) => {
                    this.beginInput();
                    UltravioletUI.Toast.quickShow({
                        content: i18next.t("mediawiki:error.apiError"),
                    });
                    Log.error("Failed to run `renderMain`.", e);
                });
        } else {
            this.replaceMain(newMain);
            this.state = MaterialInputCardState.Ready;
        }
    }

    /**
     * Called whenever the content of the input card has been modified. If the
     * returned value is a promise, a loading screen will be shown first before
     * the main content is rendered to the dom. If the card was created without
     * a default value, this is not called when the card is rendered.
     *
     * The returned value of this function should be disposable; they are not
     * supposed to be stored or kept as they will be deleted after re-render.
     */
    abstract renderMain(value: string): PromiseOrNot<HTMLDivElement>;
}
