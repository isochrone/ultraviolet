import { MaterialUserSelectProps } from "app/styles/material/ui/components/MaterialUserSelect";
import { h } from "@10nm/tsx-dom";
import { MaterialReportingDialogChildProps } from "app/styles/material/ui/components/MaterialReportingDialogChild";
import UVUIElement from "app/ui/elements/UVUIElement";
import MaterialSelect, {
    MaterialSelectElement,
    MaterialSelectItem,
} from "app/styles/material/ui/components/MaterialSelect";
import {
    isEmailReportVenue,
    isPageReportVenue,
    ReportVenue,
} from "app/mediawiki/report/ReportVenue";
import i18next from "i18next";
import MaterialTextInput, {
    MaterialTextInputComponents,
    MaterialTextInputUpgrade,
} from "app/styles/material/ui/components/MaterialTextInput";
import { ClientUser, Page, User } from "app/mediawiki";

class MaterialReportingDialogInfo extends UVUIElement {
    get venue(): ReportVenue {
        return this.props.reportingDialog.venue;
    }

    get target(): User | Page {
        return this.props.reportingDialog.target;
    }
    get subject(): string {
        return this.props.reportingDialog.subject;
    }
    set subject(value: string) {
        this.props.reportingDialog.subject = value;
    }
    get reason(): string {
        return this.props.reportingDialog.reason;
    }
    set reason(value: string) {
        this.props.reportingDialog.reason = value;
    }
    get comments(): string {
        return this.props.reportingDialog.comments;
    }
    set comments(value: string) {
        this.props.reportingDialog.comments = value;
    }

    elements: {
        root?: JSX.Element;
        subject?: MaterialTextInputComponents;
        dropdown?: MaterialSelectElement<string>;
        comments?: MaterialTextInputComponents;
    } = {};

    constructor(
        readonly props: MaterialUserSelectProps &
            MaterialReportingDialogChildProps
    ) {
        super();

        this.comments =
            (isEmailReportVenue(this.venue) &&
                this.venue.prefill
                    .replace(/{{{url}}}/g, window.location.href)
                    .replace(
                        /{{{clean-url}}}/g,
                        new URL(
                            (this.props.reportingDialog.target as Page).url,
                            window.location.href
                        ).toString()
                    )
                    .replace(/{{{username}}}/g, ClientUser.i.username)) ||
            "";
    }

    renderSubject(): JSX.Element {
        if (isEmailReportVenue(this.venue)) {
            const targetString =
                this.target instanceof User
                    ? this.target.username
                    : this.target.title.getPrefixedText();
            const el = (
                <MaterialTextInput
                    class="uv-mdc-reportingDialog--subject"
                    label={i18next.t<string>("ui:reporting.subject.label")}
                    defaultText={this.venue.subject.replace(
                        /{{{target}}}/g,
                        targetString
                    )}
                />
            ) as ReturnType<typeof MaterialTextInput>;
            this.elements.subject = MaterialTextInputUpgrade(el);
            this.elements.subject.textField.listen("input", () => {
                this.subject = this.elements.subject.textField.value;
            });

            return el;
        } else {
            return null;
        }
    }

    renderDropdown(): JSX.Element {
        if (isPageReportVenue(this.venue)) {
            return (this.elements.dropdown = (
                <MaterialSelect<string>
                    label={i18next.t("ui:reporting.info.reason.label")}
                    items={
                        [
                            {
                                type: "action",
                                label: i18next.t(
                                    "ui:reporting.info.reason.other"
                                ),
                                value: null,
                            },
                            {
                                type: "header",
                                label: i18next.t(
                                    "ui:reporting.info.reason.default"
                                ),
                            },
                            ...this.venue.defaultReasons.map((reason) => ({
                                type: "action",
                                label: reason,
                                value: reason,
                            })),
                        ] as MaterialSelectItem<string>[]
                    }
                    onChange={(_, value) => {
                        this.elements.comments.textField.required =
                            value === null;
                        this.reason = value;
                        this.props.reportingDialog.uiValidate();
                    }}
                    required
                />
            ) as MaterialSelectElement<string>);
        } else {
            return null;
        }
    }

    renderCommentsBox(): JSX.Element {
        this.elements.comments = MaterialTextInputUpgrade(
            <MaterialTextInput
                label={i18next.t("ui:reporting.info.comments.label")}
                helperText={i18next.t("ui:reporting.info.comments.placeholder")}
                defaultText={this.comments}
                outlined
                area
                required
            />
        );

        this.elements.comments.textField.listen("input", () => {
            this.comments = this.elements.comments.textField.value;
            this.props.reportingDialog.uiValidate();
        });

        return this.elements.comments.element;
    }

    render(): JSX.Element {
        this.elements.root = (
            <div class="uv-mdc-reportingDialog-info">
                {this.renderSubject()}
                {this.renderDropdown()}
                {this.renderCommentsBox()}
            </div>
        );

        return this.elements.root;
    }
}

export { MaterialReportingDialogInfo as MaterialReportingDialogInfoController };
export default function generator(
    props: MaterialReportingDialogChildProps
): JSX.Element & { MRDInfo: MaterialReportingDialogInfo } {
    const mrdReasonInfo = new MaterialReportingDialogInfo(props);
    return Object.assign(mrdReasonInfo.render(), {
        MRDInfo: mrdReasonInfo,
    });
}
