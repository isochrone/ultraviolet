export default class UltravioletIDBError extends Error {
    constructor(
        message: string,
        public database?: IDBDatabase,
        public transaction?: IDBTransaction,
        public request?: IDBRequest
    ) {
        super(message);
    }
}
