// Do not import any other file except the build information.
import buildinfo from "!webpack-plugin-buildinfo?gitHash&gitHashShort&time&platform&arch!./buildinfo.js";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const packageInfo = require(__dirname + "/../../package.json");

export const APP_BUILDINFO = buildinfo;

// These need to be updated manually.
export const APP_VERSION = packageInfo.version;
export const APP_VERSION_TAG = `${APP_VERSION}${
    process.env.NODE_ENV === "development"
        ? `+${APP_BUILDINFO.gitHashShort}`
        : ""
}`;

// UV
export const APP_LOGO = "https://w.wiki/55Qr";
// Ultraviolet
export const APP_WORDRMARK = "https://w.wiki/55Qs";

export const APP_CONFIG_VERSION = 1;
export const APP_DATABASE_NAME = "ultravioletDB";
export const APP_DATABASE_VERSION = 1;

export const APP_LOG_SIGNATURE = `Ultraviolet ${APP_VERSION_TAG}`;

export const APP_SIGNATURE = "~~~~";

export const APP_WIKI_CONFIGURATION_PAGES = [
    "MediaWiki:Ultraviolet-configuration.json",
    "Project:Ultraviolet/configuration.json",
    // TODO: Remove in a future version - kept for backwards compatibility.
    "Project:RedWarn/configuration.json",
];
export const APP_WIKI_CONFIGURATION_VERSION = 1;

export const NOWIKI_OPEN = atob("PG5vd2lraT4=");
export const NOWIKI_CLOSE = atob("PC9ub3dpa2k+");

export const APP_WIKIS_SPEEDUP = ["enwiki"];

// TODO: Host global configuration file on Meta instead of enwiki
export const APP_FALLBACK_WIKI = {
    indexPath: "https://en.wikipedia.org/w/index.php",
    apiPath: "https://en.wikipedia.org/w/api.php",
};

// TODO: Remove reliance on fallback config using defaults.
export const APP_FALLBACK_CONFIG =
    "https://en.wikipedia.org/wiki/Wikipedia:Ultraviolet/configuration.json?action=raw&ctype=application/json";
