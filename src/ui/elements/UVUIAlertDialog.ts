import { ComponentChild } from "@10nm/tsx-dom";
import {
    UVUIDialog,
    UVUIDialogAction,
    UVUIDialogProperties,
} from "app/ui/elements/UVUIDialog";

export interface UVUIAlertDialogProps extends UVUIDialogProperties {
    /**
     * The actions of the dialog. These go at the bottom of the dialog.
     */
    actions: UVUIDialogAction[];
    /**
     * The content of the dialog.
     */
    content?: ComponentChild;
    /**
     * Optional raw content, for errors etc. Will be wrapped in a <pre>
     */
    preformattedContent?: string;
}

export class UVUIAlertDialog extends UVUIDialog<string> {
    show(): Promise<string> {
        throw new Error("Attempted to call abstract method");
    }
    render(): HTMLDialogElement {
        throw new Error("Attempted to call abstract method");
    }

    public static readonly elementName = "uvAlertDialog";

    constructor(readonly props: UVUIAlertDialogProps) {
        super(props);
    }
}
