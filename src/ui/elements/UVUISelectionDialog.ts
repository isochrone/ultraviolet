import { UVUIDialog, UVUIDialogProperties } from "app/ui/elements/UVUIDialog";

export interface UVUISelectionDialogItem {
    icon?: string;
    iconColor?: string;
    color?: string;
    content: string;
    data: string;
    action?: (event: Event) => any;
}

export interface UVUISelectionDialogProps extends UVUIDialogProperties {
    title: string;
    items: UVUISelectionDialogItem[];
}

export class UVUISelectionDialog extends UVUIDialog<string> {
    show(): Promise<string> {
        throw new Error("Attempted to call abstract method");
    }
    render(): HTMLDialogElement {
        throw new Error("Attempted to call abstract method");
    }

    public static readonly elementName = "uvSelectionDialog";

    constructor(readonly props: UVUISelectionDialogProps) {
        super(props);
    }
}
