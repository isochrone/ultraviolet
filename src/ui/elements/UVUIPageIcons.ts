import UVUIElement from "app/ui/elements/UVUIElement";

/**
 * The UVUIPageIcons handle the page customization icons found in the top-right
 * corner of article pages.
 *
 * Note that UVUIPageIcons are not used if the user has set Ultraviolet's options to
 * appear in the sidebar or portlet.
 */
export class UVUIPageIcons extends UVUIElement {
    public static readonly elementName = "uvPageIcons";

    /**
     * This element, as returned by {@link UVUIPageIcons.render}.
     */
    self: HTMLElement;

    /**
     * Renders the page icons. These are then wrapped by an Ultraviolet container
     * element to isolate other elements.
     *
     * This is called only once: on insertion. Any subsequent expected changes
     * to this element will be called through other functions.
     */
    render(): JSX.Element {
        return undefined;
    }
}
