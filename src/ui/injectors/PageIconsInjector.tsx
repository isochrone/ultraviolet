import { h } from "@10nm/tsx-dom";
import UltravioletUI from "app/ui/UltravioletUI";

import "../css/pageIcons.css";
import { Injector } from "./Injector";
import classMix from "app/styles/material/util/classMix";
import UltravioletHooks from "app/event/UltravioletHooks";

export default class PageIconsInjector implements Injector {
    /**
     * Initialize the injector. If the page is a diff page, this injector
     * will trigger.
     */
    async init(): Promise<void> {
        const firstHeading = document.getElementById("firstHeading");

        const pageIconsMain = new UltravioletUI.PageIcons();
        const pageIcons = (
            <div
                id={"uvPageIcons"}
                class={classMix(
                    // `true` if not displayed
                    firstHeading.offsetParent == null &&
                        "uv-firstHeading--hidden"
                )}
            >
                {pageIconsMain.render()}
            </div>
        );

        const target =
            document.querySelector(".mw-indicators") ??
            // Fallback to article title.
            firstHeading;
        target.insertAdjacentElement("beforebegin", pageIcons);

        UltravioletHooks.addHook("deinit", () => {
            pageIcons.parentElement?.removeChild(pageIcons);
        });
    }
}
