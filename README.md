# Ultraviolet

Ultraviolet - made with TypeScript, Webpack, and TSX-DOM.

[Ultraviolet](https://en.wikipedia.org/wiki/WP:UV) is a [MediaWiki](https://mediawiki.org/wiki/) [patrol](https://en.wikipedia.org/wiki/WP:RCP) and [counter-vandalism](https://en.wikipedia.org/wiki/WP:VD) tool, designed to be a user-friendly way to perform common moderation tasks. Ultraviolet is a fully rewritten version of RedWarn (the **R**ecent **Ed**its Patrol and **Warn**ing Tool).

You can help! If you find any bugs or would like new features, you can fix these or add them yourself. More technical documentation is coming in the user guide soon to help ease this process.

## Contributors

Ultraviolet is primarily maintained and developed by The Ultraviolet Development Team (10nm).

-   **[@ChlodAlejandro](https://gitlab.wikimedia.org/chlod)** - ([\[\[User:Chlod\]\]](https://en.wikipedia.org/wiki/User:Chlod)) - project lead, development
-   **[@sportshead](https://gitlab.wikimedia.org/sportz)** - ([\[\[User:Sportzpikachu\]\]](https://en.wikipedia.org/wiki/User:Sportzpikachu)) - assistant project lead, development
-   **[@pr0mpted](https://gitlab.com/pr0mpted)** - ([\[\[User:Prompt0259\]\]](https://en.wikipedia.org/wiki/User:Prompt0259)) - development
-   **[@leijurv](https://gitlab.com/leijurv)** - ([\[\[User:Leijurv\]\]](https://en.wikipedia.org/wiki/User:Leijurv)) - development and advice
-   **[@Remagoxer](https://gitlab.wikimedia.org/regox)** - ([\[\[User:Remagoxer\]\]](https://en.wikipedia.org/wiki/User:Remagoxer)) - development
-   **[@Asartea](https://gitlab.wikimedia.org/asartea)** - ([\[\[User:Asartea\]\]](https://en.wikipedia.org/wiki/User:Asartea)) - documentation
-   **[@Sennecaster](https://gitlab.com/Sennecaster)** - ([\[\[User:Sennecaster\]\]](https://en.wikipedia.org/wiki/User:Sennecaster)) - documentation
-   **[@isochrone](https://gitlab.wikimedia.org/isochrone)** - ([\[\[User:Isochrone\]\]](https://en.wikipedia.org/wiki/User:Isochrone)) - documentation
-   **[@ed_e](https://gitlab.com/ed_e)** ([\[\[User:Ed6767\]\]](https://en.wikipedia.org/wiki/User:Ed6767)) - original creator
-   **[and everyone else on the Ultraviolet development team (10nm)](https://en.wikipedia.org/wiki/Wikipedia:Ultraviolet/Team)**
-   **[with some additional development from Wikipedia editors](https://en.wikipedia.org/wiki/WP:UV#Team)**
-   **[and other contributors.](https://gitlab.wikimedia.org/repos/10nm/ultraviolet/-/graphs/master)**

## Development

If you wish to contribute in the development of Ultraviolet, follow the instructions on our [development wiki](https://redwarn.toolforge.org/wiki/Development).

## License

Copyright © 2020–2023 The Ultraviolet Development Team (10nm) and contributors. Released under the Apache License 2.0. Transpiled versions on Wikimedia Foundation wikis are sublicensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License, and GNU Free Documentation License 1.3. See [LICENSE](/LICENSE) for more information.
